﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public Camera PostCamera;
    public Camera myCamera;
    public float WIDTH;
    public float frequency = 2;

    public GameObject cursor;

    private float score = 0;
    public TextMesh scoreText;
    private int multiplier = 1;
    public TextMesh multiplierText;
    public float gauge = 0f;
    public Material gaugeMaterial;

    public AudioSource AudioScore;
    public AudioSource AudioMultiplier;
    public AudioSource AudioBreak;
    public AudioSource AudioGameover;
    public ParticleSystem BreakParticles;

    private float lastSpawn = 0;
    private bool gameover = false;

    // Use this for initialization
    void Start () {
        Cursor.visible = false;
        gaugeMaterial.SetFloat("_Filled", 0);
        
    }
	
	// Update is called once per frame
	void Update () {
        if (!gameover) {

            frequency = 2 + (multiplier * 0.01f);


            if (Input.GetKeyDown(KeyCode.M)) {
                if (this.GetComponent<AudioSource>().volume != 0) {
                    this.GetComponent<AudioSource>().volume = 0;
                } else {
                    this.GetComponent<AudioSource>().volume = 0.5f;
                }
            }

            //Check for new spawns
            if ((Time.time - lastSpawn) >= 1 / frequency) {
                Spawn();
                lastSpawn = Time.time;
            }

            //Update score
            int previous = (int)score;
            score += multiplier * Time.deltaTime;
            scoreText.text = ((int)score).ToString();
            if (previous != (int)score) {
                AudioScore.Play();
            }

            //Update multiplier?
            multiplierText.text = "x" + multiplier.ToString();

            //Update mouse position
            Vector3 mousePos = PostCamera.ScreenToWorldPoint(Input.mousePosition);
            mousePos += new Vector3(10000, 0, 0);
            mousePos.z = 0;

            //Update cursor rotation and position
            if (Vector3.Magnitude(mousePos - cursor.transform.position) > 0.01f) {
                cursor.transform.rotation = Quaternion.LookRotation(Vector3.Normalize(mousePos - cursor.transform.position), new Vector3(0, 0, -1));
            }
            cursor.transform.position = mousePos;
        }
	}

    void Spawn() {
        if (Random.Range(0.0f, 1.0f) >= 0.66f) {
            SpawnBad();
        } else {
            SpawnGood();
        }
    }

    void SpawnBad() {
        GameObject newBall = (GameObject) Instantiate(Resources.Load("BadBall"));
        newBall.GetComponent<Ball>().gc = this;
        newBall.GetComponent<Ball>().isGood = false;
        newBall.transform.position = this.transform.position + new Vector3(Random.Range(-WIDTH, WIDTH), 0, 0);
        //newBall.transform.rotation = Quaternion.Euler(new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)));
    }

    void SpawnGood() {
        GameObject newBall = (GameObject)Instantiate(Resources.Load("GoodBall"));
        newBall.GetComponent<Ball>().gc = this;
        newBall.GetComponent<Ball>().isGood = true;
        newBall.transform.position = this.transform.position + new Vector3(Random.Range(-WIDTH, WIDTH), 0, 0);
        //newBall.transform.rotation = Quaternion.Euler(new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)));
    }

    public void OnBadPassed(Vector3 position) {
        multiplier = 1;
        AudioMultiplier.pitch = 0.75f;
        gauge += 0.3f;
        gaugeMaterial.SetFloat("_Filled", gauge);

        //Break Combo Sound
        AudioBreak.Play();

        //Spawn Bad Particles
        GameObject badParticles = (GameObject)Instantiate(Resources.Load("BadParticles"));
        badParticles.transform.position = position;
        //Spawn Multiplier Broken Particles
        GameObject multiplierParticles = (GameObject)Instantiate(Resources.Load("MultiplierBrokenParticles"));
        multiplierParticles.transform.position = multiplierText.transform.position + new Vector3(0, 0, -0.2f) ;

        //Is it Game Over?
        if (gauge >= 1 && !gameover) {
            OnGameOver();
            gameover = true;
        }
    }

    public void OnGoodPassed(Vector3 position) {
        multiplier += 1;

        //Combo Up Sound
        AudioMultiplier.pitch += 0.05f;
        AudioMultiplier.Play();

        //Spawn Good Particles
        GameObject goodParticles = (GameObject)Instantiate(Resources.Load("GoodParticles"));
        goodParticles.transform.position = position;
    }


    void OnGameOver() {
        GameObject scoreManager = GameObject.Find("Score Manager");
        if (scoreManager) {
            scoreManager.GetComponent<ScoreManager>().commitScore(score);
        }
        gameover = true;
        StartCoroutine("GameOverCinematic");
        //Play Heavy Sound
        AudioGameover.Play();
        //Spawn GameOver Particles
        GameObject gameoverParticles = (GameObject)Instantiate(Resources.Load("GameOverParticles"));
        gameoverParticles.transform.position = new Vector3(0,-2.5f,0);
        //Fade to black
    }

    IEnumerator GameOverCinematic() {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("NewScoreScene");
    }
}
