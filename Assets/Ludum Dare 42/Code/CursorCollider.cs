﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorCollider : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision collision) {
        this.GetComponent<AudioSource>().pitch = Random.Range(1.8f, 2.2f);
        this.GetComponent<AudioSource>().Play();
        GameObject badParticles = (GameObject)Instantiate(Resources.Load("CollisionParticles"));
        badParticles.transform.position = collision.contacts[0].point;
        badParticles.transform.rotation = Quaternion.LookRotation(Vector3.Normalize(collision.collider.transform.position - this.transform.position));
        collision.collider.GetComponent<Rigidbody>().AddExplosionForce(10, this.transform.position, 1.0f);
    }
}
