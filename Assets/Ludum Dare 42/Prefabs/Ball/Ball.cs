﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    public GameController gc;
    public bool isGood = false;

    void Update() {
        if(this.transform.position.y < -10) {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter(Collider other) {
        if (isGood) {
            gc.OnGoodPassed(this.transform.position);
        } else {
            gc.OnBadPassed(this.transform.position);
        }
        StartCoroutine("DestroySelf");
    }

    IEnumerator DestroySelf() {
        yield return new WaitForSeconds(1);
        Destroy(this.gameObject);
    }
}
