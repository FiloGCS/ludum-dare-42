﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButton : MonoBehaviour {
    public string action;

    public void Update() {
        GetComponent<TextMesh>().color = new Color(0.66f,0.66f,0.66f);
    }

    public void highlight() {
        this.GetComponent<TextMesh>().color = new Color(1f, 1f, 1f);
    }
}
