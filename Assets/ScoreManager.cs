﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    private float hiscore = 0;
    private string scoreName = "null";
    
	void Start () {
        DontDestroyOnLoad(this.gameObject);
	}

    public void commitScore(float newScore) {
        hiscore = newScore;
    }

    public float getScore() {
        return hiscore;
    }

    public void Publish(string newScoreName) {
        scoreName = newScoreName;
        StartCoroutine("AddScore");
    }

    IEnumerator AddScore() {
        /*
        IEnumerator e = DCP.RunCS("Scoreboard", "AddScoreOrdered", new string[2] {scoreName, ((int)hiscore).ToString()});
        while (e.MoveNext()) {
            yield return e.Current;
        }
        string returnText = e.Current as string;
        Debug.Log("Result " + returnText);
        GameObject menuController = GameObject.Find("Menu Controller");
        menuController.GetComponent<MenuController>().OnPublished(returnText);
        */
        yield return new WaitForSeconds(0.1f);
    }

}