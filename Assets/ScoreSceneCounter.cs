﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreSceneCounter : MonoBehaviour {
    
	void Start () {
        GameObject scoreManager = GameObject.Find("Score Manager");
        this.GetComponent<TextMesh>().text = ((int)scoreManager.GetComponent<ScoreManager>().getScore()).ToString();
	}
}
