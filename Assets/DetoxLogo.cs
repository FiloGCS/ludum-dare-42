﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetoxLogo : MonoBehaviour {
    
	// Update is called once per frame
	void Update () {
        //float scale = 115 * (1.0f + Mathf.Cos(Time.time*3) * 0.1f);
        //this.transform.localScale = new Vector3(scale,scale,scale);
        float rotationx = Mathf.Cos(Time.time * 0.75f) * 10.0f;
        float rotationz = Mathf.Cos(Time.time * 0.45f) * 15.0f;
        this.transform.rotation = Quaternion.Euler(new Vector3(rotationx, rotationz + 159.55f , 0));
    }
}
