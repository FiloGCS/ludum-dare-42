﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    public Camera PostCamera;
    public Camera myCamera;
    
    public GameObject reticle;
    public TextMesh scoreText;

    public GameObject publishButton;
    public GameObject cancelButton;
    public GameObject uploadingLabel;
    public GameObject loadingScoresLabel;
    public GameObject Scoreboard;
    public List<TextMesh> scoreboardNames;
    public List<TextMesh> scoreboardScores;
    public Text playerName;

    private GameObject lastHighlighted = null;

    // Use this for initialization
    void Start () {
        Cursor.visible = false;
        if(SceneManager.GetActiveScene().name == "RankingsScene") {
            StartCoroutine("GetScores");
        }
	}
	
	void Update () {
        
        //Update reticle position
        Vector3 mousePos = PostCamera.ScreenToWorldPoint(Input.mousePosition);
        mousePos += new Vector3(10000, 0, 0);
        mousePos.z = 0;
        reticle.transform.position = mousePos;

        //Highlight
        RaycastHit hit;
        Ray ray = new Ray(mousePos, new Vector3(0, 0, 1));
        if(Physics.Raycast(ray, out hit)) {
            if (hit.collider.GetComponent<MenuButton>() != null) {
                hit.collider.GetComponent<MenuButton>().highlight();
                if(hit.collider.gameObject != lastHighlighted) {
                    //play sound
                    this.GetComponent<AudioSource>().Play();
                    print("ASD");
                    lastHighlighted = hit.collider.gameObject;
                }
                if (Input.GetMouseButtonDown(0)) {
                    switch (hit.collider.GetComponent<MenuButton>().action) {
                        case "start":
                            DestroyScoreManager();
                            SceneManager.LoadScene("PlayScene");
                            break;
                        case "onlinehiscores":
                            SceneManager.LoadScene("RankingsScene");
                            break;
                        case "howtoplay":
                            SceneManager.LoadScene("HowtoScene");
                            break;
                        case "back":
                            SceneManager.LoadScene("MenuScene");
                            break;
                        case "publishscene":
                            SceneManager.LoadScene("PublishScoreScene");
                            break;
                        case "publish":
                            //Tell the scoreManager to publish
                            GameObject scoreManager = GameObject.Find("Score Manager");
                            scoreManager.GetComponent<ScoreManager>().Publish(playerName.text); //TODO custom name
                            //Hide Publish and cancel buttons
                            publishButton.SetActive(false);
                            cancelButton.SetActive(false);
                            uploadingLabel.SetActive(true);
                            //Now we wait...
                            break;
                        case "exit":
                            Application.Quit();
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }

    void DestroyScoreManager() {
        GameObject scoreManager = GameObject.Find("Score Manager");
        if (scoreManager) {
            Destroy(scoreManager);
        }
    }

    public void OnPublished(string result) {
        switch (result) {
            case "newscore":
                SceneManager.LoadScene("RankingsScene");
                break;
            case "notnewscore":
                SceneManager.LoadScene("RankingsScene");
                break;
            default:
                //Hide Publish and cancel buttons
                publishButton.SetActive(true);
                cancelButton.SetActive(true);
                uploadingLabel.SetActive(false);
                break;
        }
    }

    IEnumerator GetScores() {
        yield return new WaitForSeconds(0.1f);
        /*
        IEnumerator e = DCP.RunCS("Scoreboard", "GetScores");
        while (e.MoveNext()) {
            yield return e.Current;
        }
        string returnText = e.Current as string;
        GameObject menuController = GameObject.Find("Menu Controller");
        OnReceivedScoreboard(returnText);
        */
    }

    public void OnReceivedScoreboard(string result) {
        //Tokenize the result
        string[] tokenized = result.Split('/');
        //If it's valid
        if(tokenized[0] == "Success") {
            //For each token
            for(int i = 1; i < tokenized.Length; i++) {
                print(i);
                if (i % 2 != 0) {//NAME
                    scoreboardNames[(i / 2)].text = tokenized[i];
                } else {//SCORE
                    scoreboardScores[(i / 2) - 1].text = tokenized[i];
                }
            }
        }

        //Change UI
        loadingScoresLabel.SetActive(false);
        Scoreboard.SetActive(true);
    }
}
