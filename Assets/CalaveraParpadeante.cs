﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalaveraParpadeante : MonoBehaviour {

    public GameController gc;

	void Update () {
        if (gc.gauge > 0.7f) {
            this.GetComponent<SpriteRenderer>().color = Color.Lerp(Color.white, Color.gray, (Mathf.Cos(Time.time * 5) + 1) / 2);
        }
	}
}
